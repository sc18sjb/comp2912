<?php
session_start();
require_once "dbh.inc.php";
require_once "classes.inc.php";

if (Input::exists('deleteStudent')) {
    $student = new StudentUser();
    
    try {
        $student->delete('tblstudent', array(
            'studentID',
            '=',
            $_POST['id']
        ));
        header("Location: ../app/parent/accounts?delete=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
} else if (Input::exists('deleteCard')) {
    $card = new Billing();

    try {
         $card->delete('tblbilling', array(
             'billingID',
             '=',
             $_POST['id']
         ));
         Session::delete('card');
         header("Location: ../app/parent/billing?delete=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
} else if (Input::exists('deleteActivity')) {
    $activity = new Activity();

    try {
        $activity->delete('tblactivity', array(
            'activityID',
            '=',
            $_POST['id']
        ));
        header("Location: ../app/student/activities?delete=successs");
    } catch (Exception $e) {
        die($e->getMessage());
    }
} else if (Input::exists('deleteParent')) {
    $parent = new ParentUser();

    try {
        $parent->delete('tblparent', array(
            'parentID',
            '=',
            Session::get('user')
        ));
        header("Location: ../index?delete=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
}