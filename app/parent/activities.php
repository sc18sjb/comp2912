<?php
    $pageName = "parent | Accounts";
    $header = "parent";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $parent = new ParentUser();
        $user = $parent->getRows('tblParent', array(
            'parentID',
            '=',
            Session::get('user')
        ))[0];

        $accounts = $parent->getRows('tblstudent', array(
            'parentID',
            '=',
            $user->parentID
        ));
    } else {
        header("Location: ../../index?user=undefined");
    }
?>

<div class="container">
    <br />
    <h1><?php echo $user->parentFirst; ?>'s Activities</h1>

    <br />

    <?php
        if ($accounts) {
            if (count($accounts) > 1) {
    ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Select User</h5>
            <form action="" method="GET" autocomplete="off">
                <div class="form-group">
                    <select class="form-control" name="user">
                        <?php
                            foreach ($accounts as $int => $account) {
                        ?>

                        <option value="<?php echo $int; ?>"><?php echo $account->studentFirst; ?></option>
                        
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <button type="submitUser" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <br />

    <?php
            }
            $student = $accounts[Input::get('user', 0)];
    ?>

    <div class="row">
        <?php
            $allActivity = new Activity();
            $activities = $allActivity->getRows('tblactivity', array(
                'studentID',
                '=',
                $student->studentID
            ));

            if ($activities) {
                foreach ($activities as $int => $activity) {    
        ?>
        <div class="col-sm-6">
            <div class="card">
                <img src="/comp2912/uploads/<?php echo $activity->activityImage; ?>" class="card-img-top" style="width: 100%; height: 15vw; object-fit: cover;">
                <div class="card-body">
                    <h5 class="card-title">Activity <?php echo $int + 1; ?></h5>
                    <p class="card-text"><?php echo $activity->activityDsc; ?></p>
                    <a href="#" class="btn btn-primary">Where were they?</a>
                </div>
            </div>
            <br />
        </div>

        <?php
                }
            } else {
        ?>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $student->studentFirst; ?> has no activities</h5>
                    <p class="card-text"><?php echo $student->studentFirst; ?> has not added any activities to their account, remind them to add some!</p>
                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                </div>
            </div>
            <br />
        </div>

        <?php
            }
        ?>
    </div>


    <?php
        }        
    ?>


    



</div>

<?php
    require_once "../../includes/footer.inc.php";
?>