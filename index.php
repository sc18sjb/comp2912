<?php
    $pageName = "Home";

    require_once "includes/header.inc.php";
?>

<div class="container">
    <br />
    <h1> Student Tracker System </h1>
    <a href="login">Login</a>
    <br />
    <a href="register">Register</a>
</div>

<?php
    require_once "includes/footer.inc.php";
?>