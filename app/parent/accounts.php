<?php
    $pageName = "parent | Accounts";
    $header = "parent";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $parent = new ParentUser();
        $user = $parent->getRows('tblParent', array(
            'parentID',
            '=',
            Session::get('user')
        ))[0];
    } else {
        header("Location: ../../index?user=undefined");
    }
?>


<div class="container">
    <br />
    <h1><?php echo $user->parentFirst; ?>'s Accounts</h1>

    <form action="../../includes/delete.inc.php", method="POST", autocomplete="off">
        <!-- <input type="hidden" name="id" value="<?php echo $account->studentID; ?>"></input> -->
        <button type="submit" class="btn btn-primary" name="deleteParent">Delete Your Account</button>
    </form>

    <br />
    <h4> Your Users </h4>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Email</th>
                    <th scope="col">Password</th>
                    <th scope="col"></th>
                </tr>
            </thead>

            <tbody>
                <?php
                    $accounts = $parent->getRows('tblstudent', array(
                        'parentID',
                        '=',
                        $user->parentID
                    ));

                    if ($accounts) {
                        foreach ($accounts as $int => $account) {                
                ?>

                <tr>
                    <th scope="row"><?php echo $int + 1; ?></th>
                    <td><?php echo $account->studentFirst; ?></td>
                    <td><?php echo $account->studentLast; ?></td>
                    <td><?php echo $account->studentEmail; ?></td>
                    <td><?php echo $account->studentPwd; ?></td>
                    <td>
                        <form action="../../includes/delete.inc.php", method="POST", autocomplete="off">
                            <input type="hidden" name="id" value="<?php echo $account->studentID; ?>"></input>
							<button type="submit" class="btn btn-primary" name="deleteStudent">Delete</button>
						</form>
                    </td>
                </tr>
            
                <?php
                        }
                    } else {
                        
                ?>

                <tr>
                    <th scope="row">1</th>
                    <td>Example</td>
                    <td>User</td>
                    <td>example@user.com</td>
                    <td>ExamplePwd</td>
                </tr>

                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>

    <br />
    <h4> Add New Users </h4>
    <form action="../../includes/register.inc.php", method="POST", autocomplete="off">
        <div class="form-group">
            <label for="exampleInputEmail1">Firstname</label>
            <input type="text" class="form-control" name="first" placeholder="Enter firstname">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Surname</label>
            <input type="text" class="form-control" name="last" placeholder="Enter surname">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">University Email address</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="pwd" placeholder="Enter Password">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" class="form-control" name="confirmPwd" placeholder="Enter Password Again">
        </div>

        <button type="submit" class="btn btn-primary" name="submitStudent">Create Account</button>
    </form>
    <br />
    <br />
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>