<?php
    $pageName = "Register";

    require_once "includes/header.inc.php";
?>

<div class="container">
    <br />
    <h1> Register Your Card </h1>


    <form action="includes/register.inc.php", method="POST", autocomplete="off">
        <div class="form-group">
            <label for="exampleInputEmail1">Cardholder Name (full)</label>
            <input type="text" class="form-control" name="name" placeholder="Enter exactly as on card">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Card Number</label>
            <input type="text" class="form-control" name="number" placeholder="#### #### #### ####">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Card Expiery</label>
            <input type="date" class="form-control" name="expire" placeholder="##/##/##">
        </div>

        <button type="submit" class="btn btn-primary" name="submitCard">Add Card</button>
    </form>


    <a href="app/parent/dashboard">Skip (add later)</a>
    <br />
    <a href="index">Home</a>

</div>

<?php
    require_once "includes/footer.inc.php";
?>