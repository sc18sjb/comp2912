<?php

class User {
    protected $dbh;

    function __construct() {
        $this->setDbh(new Dbh());
    }

    private function setDbh($dbh) {
        $this->dbh = $dbh;
    }

    public function find($table, $fields) {
        if ($fields) {
            $data = $this->dbh->get('*', $table, $fields);

            if ($data->getCount()) {
                return $data->first();
            }
        }
        return false;
    }

    public function create($table, $fields = array(), $session = "") {
        if (!$this->dbh->insert($table, $fields)) {
            throw new Exception("There was a problem processing your request.");
        } else if ($session != "") {
            Session::put($session, $this->dbh->getLastId());
        }
    }

    public function login($table, $email = array(), $pwd = array()) {
        $fields = array($email, $pwd);
        $user = $this->find($table, $fields);

        if ($user) {
            return true;
        }
        return false;
    }

    public function delete($table, $fields) {
        if (!$this->dbh->delete($table, $fields)) {
            throw new Exception("There was a problem deleting the account");
        }
    }

    public function getRows($table, $fields) {
        $rows = $this->dbh->get('*', $table, $fields);
        
        if ($rows->getCount()) {
            return $rows->getResults();
        }
        return false;
    }
}


class ParentUser extends User{
    public function login($table, $email = array(), $pwd = array()) {
        $fields = array($email, $pwd);
        $user = $this->find($table, $fields);

        if ($user) {
            Session::put('user', $user->parentID);
            // check if they have a card set to their account 

            $cards = $this->find('tblbilling', array(
                'parentID',
                '=',
                $user->parentID
            ));
            
            if ($cards) {
                Session::put('card', $cards->billingID);
            }
            return true;
        }
        return false;
    }
}


class StudentUser extends User {
    public function login($table, $email = array(), $pwd = array()) {
        $fields = array($email, $pwd);
        $user = $this->find($table, $fields);

        if ($user) {
            Session::put('user', $user->studentID);
            return true;
        }
        return false;
    }

    public function create($table, $fields = array(), $session = "") {
        $result = $this->dbh->insert($table, $fields);
        $modules = array(1, 2, 3, 4, 5, 6);
        $id = $this->dbh->getLastId();

        if ($result) {
            foreach ($modules as $module) {
                if (!$this->dbh->insert('tblgrade', array(
                    'moduleID' => $module,
                    'studentID' => $id,
                    'grade' => 'Pending',
                    'year' => '2019'
                ))) {
                    throw new Exception("There was a problem creating your user account");
                    return;
                };
            }
            
        }
        
        if (!$result) {
            throw new Exception("There was a problem creating your user account.");
        } else if ($session != "") {
            Session::put($session, $id);
        }
    }
}


class Billing extends User {

}


class Grade extends User {
    public function updateGrades($fields = array(), $mid, $sid) {
        if (!$this->dbh->update('tblgrade', $fields, array(
            array('moduleID', '=', $mid),
            array('studentID', '=', $sid)
        ))) {
            throw new Exception("There was a problem processing your request");
        }
    }
}

class Timetable extends User {

}

class Activity extends User {
    
}

class Reminder extends User {
    
}


class Input {
    public static function exists($name, $type = 'post') {
        switch($type) {
            case 'post':
                return (isset($_POST[$name])) ? true : false;
            break;  
            case 'get':
                return (isset($_GET[$name])) ? true : false;
            break; 
            default:
                return false;
            break;
        }
    }

    public static function get($item, $default = '') {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } else if (isset($_GET[$item])) {
            return $_GET[$item];
        }
        return $default;
    }
}


class Session {
    public static function exists($name) {
        return (isset($_SESSION[$name])) ? true : false;
    }

    public static function put($name, $value) {
        return $_SESSION[$name] = $value;
    }

    public static function get($name) {
        return $_SESSION[$name];
    }

    public static function delete($name) {
        if (self::exists($name)) {
            unset($_SESSION[$name]);
        }
    }
}
