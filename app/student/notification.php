<?php
    $pageName = "student | dashboard";
    $header = "student";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $student = new StudentUser();
        $user = $student->getRows('tblstudent', array(
            'studentID',
            '=',
            Session::get('user')
        ))[0];

        $parent = new ParentUser();
        $parentUser = $parent->getRows('tblparent', array(
            'parentID',
            '=',
            $user->parentID
        ))[0];
    } else {
        header("Location: ../../index?user=undefined");
    }
?>

<div class="container">
    <br />
    <h1><?php echo $user->studentFirst; ?>'s Notifications </h1>

    <br />


    <?php         
        $today = new DateTime(date('Y-m-d H:i:s'));

        $notification = new Reminder();
        $reminders = $notification->getRows('tblnotification', array(
            array('studentID', '=', $user->studentID),
            array('notificationDirection', '=', 'student'),
            array('notificationTime', '>=', $today->format('Y-m-d'))
        ));

        if ($reminders) {                
    ?>

    <div class="table-reponsive">
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Notification</th>
                    <th scope="col">Deadline Date</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    foreach ($reminders as $int => $reminder) {
                ?>
                <tr>
                    <th scope="row"><?php echo $int; ?></th>
                    <td><?php echo $reminder->notificationReminder; ?></td>
                    <td><?php echo $reminder->notificationTime ?></td>
                </tr>

                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>

    <?php
        } else {
    ?>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?php echo $parentUser->parentFirst; ?> has no reminders!</h5>
                <p class="card-text"><?php echo $parentUser->parentFirst; ?> has not sent you any reminders! Send them one instead!</p>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
            </div>
        </div>
        <br />
    </div>

    <?php
        } 
    ?>


    <h4> Add New Reminder </h4>
    
    <br />


    <form action="../../includes/register.inc.php" method="POST" autocomplete="off">  
        <div class="form-group">
            <label for="exampleInputEmail1">Reminder</label>
            <input type="text" class="form-control" name="reminder" placeholder="Whats the reminder?">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">When For?</label>
            <input type="date" class="form-control" name="time" placeholder="##/##/##">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Who For?</label>
            <input type="text" class="form-control" placeholder="<?php echo $parentUser->parentFirst . ' ' . $parentUser->parentLast; ?>" readonly>
        </div>

        <input type="hidden" name="direction" value="parent">
        <button type="submit" class="btn btn-primary" name="submitReminderStudent">Add Activity</button>
    </form>
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>