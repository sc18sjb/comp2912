<?php
session_start();
require_once "dbh.inc.php";
require_once "classes.inc.php";

if (Input::exists('updateGrade')) {
    $grades = new Grade();

    try {
        $grades->updateGrades(array(
            'grade' => $_POST['grade']
        ), $_POST['mid'], $_POST['sid']);
        header("Location: ../app/student/dashboard?update=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
    
}

