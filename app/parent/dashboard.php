<?php
    $pageName = "parent | dashboard";
    $header = "parent";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $parent = new ParentUser();
        $user = $parent->getRows('tblparent', array(
            'parentID',
            '=',
            Session::get('user')
        ))[0];
        
        
        $accounts = $parent->getRows('tblstudent', array(
            'parentID',
            '=',
            $user->parentID
        ));
    } else {
        header("Location: ../../index?user=undefined");
    }
    
    $times = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'];
    $days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
?>


<div class="container">
    <br />
    <h1><?php echo $user->parentFirst; ?>'s Dashboard</h1>

    <br />

    <?php
        if (!Session::exists('card') || !$accounts) {
    ?>

    <div class="row">
        <?php 
            if (!Session::exists('card')) {
        ?>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">You must add a payment method to your account</h5>
                    <p class="card-text">You need to add a payment and set up a plan to use the features within the app.</p>
                    <a href="billing" class="btn btn-primary">Add Payment</a>
                </div>
            </div>
        </div>
        <?php
            }
            if (!$accounts) {
        ?>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">You must add a student user to your account</h5>
                    <p class="card-text">You wont be able to make the most of the application until you add a student user.</p>
                    <a href="accounts" class="btn btn-primary">Add Student</a>
                </div>
            </div>
        </div>
        <?php 
            }
        ?>
    </div>

    <br />

    <?php
        } else {
            if (count($accounts) > 1) {
    ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Select User</h5>
            <form action="" method="GET" autocomplete="off">
                <div class="form-group">
                    <select class="form-control" name="user">
                        <?php
                            foreach ($accounts as $int => $account) {
                        ?>

                        <option value="<?php echo $int; ?>"><?php echo $account->studentFirst; ?></option>
                        
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <button type="submitUser" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <br />

    <?php
            }
            $student = $accounts[Input::get('user', 0)];

            $today = new DateTime(date('Y-m-d H:i:s'));

            $notification = new Reminder();
            $reminders = $notification->getRows('tblnotification', array(
                array('studentID', '=', $student->studentID),
                array('notificationDirection', '=', 'parent'),
                array('notificationTime', '>=', $today->format('Y-m-d'))
            ));

            if ($reminders) {  
    ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">You have notifications from <?php echo $student->studentFirst; ?>!</h5>
            
            <div class="table-reponsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Notification</th>
                            <th scope="col">Deadline Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            foreach ($reminders as $int => $reminder) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $int; ?></th>
                            <td><?php echo $reminder->notificationReminder; ?></td>
                            <td><?php echo $reminder->notificationTime ?></td>
                        </tr>

                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php
        }        
    ?>

    <br />

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $student->studentFirst; ?>'s Grades</h5>

                    <div class="table responsive">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Module Name</th>
                                    <th scope="col">Module Code</th>
                                    <th scope="col">Grade</th>
                                </tr>
                            </thead>
                
                            <tbody>
                                <?php
                                    $modules = new Grade();
                                    $grades = $modules->getRows('tblgrade', array(
                                        'studentID',
                                        '=',
                                        $student->studentID
                                    ));
                                    
                                    foreach ($grades as $int => $grade) {
                                        $module = $modules->getRows('tblmodule', array(
                                            'moduleID',
                                            '=',
                                            $grade->moduleID
                                        ))[0];
                                ?>

                                <tr>
                                    <td><?php echo $module->moduleName; ?></td>
                                    <td><?php echo $module->moduleCode; ?></td>
                                    <td><?php echo $grade->grade; ?></td>
                                </tr>

                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $student->studentFirst; ?>'s Activities</h5>

                    <?php
                        $allActivities = new Activity();
                        $activities = $allActivities->getRows('tblactivity', array(
                            'studentID',
                            '=',
                            $student->studentID
                        ));

                        if (!$activities) {
                    ?>

                    <p class="card-text"><?php echo $student->studentFirst; ?> has not added any activities to their account yet! Remind them to add some.</p>
                    <a href="activities" class="btn btn-primary">Remind Them!</a>

                    <?php
                        } else {
                            $activity = $activities[count($activities) - 1];
                    ?>

                    <p class="card-text">Recently <?php echo $student->studentFirst; ?>'s been</p>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Where they were</th>
                                    <th scope="col">What they did</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <th scope="row"></th>
                                    <td scope="col"><?php echo $activity->activityLocation; ?></td>
                                    <td scope="col"><?php echo $activity->activityDsc; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <a href="activities" class="btn btn-primary">Remind Them!</a>    

                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <br />

    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><?php echo $student->studentFirst; ?>'s Timetable</h5>
            <?php
                include "../../includes/timetable.inc.php";
            ?>
        </div>
    </div>

    <br />

    <?php 
        }
    ?> 
    

</div>

<?php
    require_once "../../includes/footer.inc.php";
?>