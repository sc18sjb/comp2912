
<?php
        $timetable = new Timetable();
        $classes = $timetable->getRows('tbltimetable', array(
            'studentID',
            '=',
            '1'
        ));
?>

<div class="table-responsive">
    <table class="table table-bordered table-sm">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Mon</th>
                <th scope="col">Tue</th>
                <th scope="col">Wed</th>
                <th scope="col">Thu</th>
                <th scope="col">Fri</th>
            </tr>
        </thead>

        <tbody>
            <?php
                foreach ($times as $time) {
            ?>

            <tr>
                <th scope="row"><?php echo $time; ?></th>
                <?php
                    foreach ($days as $day) {
                        $result = false;
                        foreach ($classes as $class) {
                            $timetableDay = new DateTime($class->timetableDate);
                            $timetableTime = new DateTime($class->timetableStart);
                            if ($timetableDay->format('D') == $day && $timetableTime->format('H:i') == $time) {
                                $result = true;
                            
                ?>

                <th scope="row"><?php echo $class->timetableLecture; ?></th>

                <?php
                            }
                        }

                        if (!$result) {
                ?>
                <th scope="row"></th>

                <?php
                        }
                    } 
                ?>
            </tr>
            
            <?php
                }
            ?>
        </tbody>
    </table>
</div>
