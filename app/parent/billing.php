<?php
    $pageName = "parent | Billing";
    $header = "parent";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $parent = new ParentUser();
        $user = $parent->getRows('tblParent', array(
            'parentID',
            '=',
            Session::get('user')
        ))[0];

    } else {
        header("Location: ../../index?user=undefined");
    }
?>


<div class="container">
    <br />
    <h1><?php echo $user->parentFirst; ?>'s Billing </h1>    

    <?php
        if (Session::exists('card')) {
    ?>

    <br />
    <h4> Your Cards </h4>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Cardholder Name</th>
                    <th scope="col">Card Number</th>
                    <th scope="col">Card Expiery</th>
                    <th scope="col"></th>
                </tr>
            </thead>

            <tbody>
                <?php
                    $billing = new Billing();
                    $cards = $billing->getRows('tblbilling', array(
                        'parentID',
                        '=',
                        $user->parentID
                    ));
                    

                    if ($cards) {
                        foreach ($cards as $int => $card) {
                ?>

                <tr>
                    <th scope="row"><?php echo $int + 1; ?></th>
                    <td><?php echo $card->billingCardName; ?></td>
                    <td><?php echo $card->billingCardNumber; ?></td>
                    <td><?php echo $card->billingCardExpire; ?></td>
                    <td>
                        <form action="../../includes/delete.inc.php", method="POST", autocomplete="off">
                            <input type="hidden" name="id" value="<?php echo $card->billingID; ?>"></input>
							<button type="submit" class="btn btn-primary" name="deleteCard">Deactivate</button>
						</form>
                    </td>
                </tr>
                
                <?php
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>

    <?php 
        } else {
    ?>

    <h4> * You must add a payment method to your account * </h4>

    <?php
        }
    ?>


    <br />
    <h4> Add New Card (Overwrite Existing) </h4>
    <form action="../../includes/register.inc.php", method="POST", autocomplete="off">
        <div class="form-group">
            <label for="exampleInputEmail1">Cardholder Name (full)</label>
            <input type="text" class="form-control" name="name" placeholder="Enter exactly as on card">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Card Number</label>
            <input type="text" class="form-control" name="number" placeholder="#### #### #### ####">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Card Expiery</label>
            <input type="date" class="form-control" name="expire" placeholder="##/##/##">
        </div>

        <button type="submit" class="btn btn-primary" name="submitCard">Add Card</button>
    </form>
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>