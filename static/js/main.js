var lat = document.getElementById("lat");
var long = document.getElementById("long");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    lat.value = "NULL";
    long.value = "NULL";
  }
}


function showPosition(position) {
  // x.innerHTML = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;
  lat.value = position.coords.latitude;
  long.value = position.coords.longitude;
}


$(document).ready(function(e) {
  getLocation();
});


function initMap() {
	var map = new google.maps.Map(document.getElementById('googleMap'), {
	  zoom: 13,
	  center: {lat: 53.805471, lng: -1.553438}
	});


	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			  
			marker = new google.maps.Marker({
				map: map,
				draggable: false,
				animation: google.maps.Animation.DROP,
				position: pos
			});

			var infowindow = new google.maps.InfoWindow({
				content:"Your Location"
			});
			
			infowindow.open(map,marker);

			map.setCenter(pos);
		}, function() {
			handleLocationError(true, infoWindow, map.getCenter());
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false, infoWindow, map.getCenter());
	}
}

function toggleBounce() {
	if (marker.getAnimation() !== null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}
