<?php
    $pageName = "Register";

    require_once "includes/header.inc.php";
?>

<div class="container">
    <br />
    <h1> Register </h1>


    <form action="includes/register.inc.php", method="POST", autocomplete="off">
        <div class="form-group">
            <label for="exampleInputEmail1">Firstname</label>
            <input type="text" class="form-control" name="first" placeholder="Enter firstname">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Surname</label>
            <input type="text" class="form-control" name="last" placeholder="Enter surname">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="pwd" placeholder="Enter Password">
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" class="form-control" name="confirmPwd" placeholder="Enter Password Again">
        </div>

        <button type="submit" class="btn btn-primary" name="submitParent">Create Account</button>
    </form>


    <a href="index">Home</a>
</div>

<?php
    require_once "includes/footer.inc.php";
?>