<?php
    $pageName = "student | activities";
    $header = "student";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $student = new StudentUser();
        $user = $student->getRows('tblstudent', array(
            'studentID',
            '=',
            Session::get('user')
        ))[0];

        $parent = new ParentUser();
        $parentUser = $parent->getRows('tblparent', array(
            'parentID',
            '=',
            $user->parentID
        ))[0];

    } else {
        header("Location: ../../index?user=undefined");
    }
?>


<div class="container">
    <br />
    <h1><?php echo $user->studentFirst; ?>'s Activities </h1>

    <br />

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Where you were</th>
                    <th scope="col">What you did</th>
                    <th scope="col">Image</th>
                    <th scope="col"></th>
                </tr>
            </thead>

            <tbody>
                <?php
                    $allActivities = new Activity();
                    $activities = $allActivities->getRows('tblactivity', array(
                        'studentID',
                        '=',
                        $user->studentID
                    ));

                    if ($activities) {
                        foreach ($activities as $int => $activity) {
                ?>

                <tr>
                    <th scope="row"><?php echo $int + 1; ?></th>
                    <td><?php echo $activity->activityLocation; ?></td>
                    <td><?php echo $activity->activityDsc; ?></td>
                    <td><?php echo $activity->activityImage; ?></td>
                    <td>
                        <form action="../../includes/delete.inc.php", method="POST", autocomplete="off">
                            <input type="hidden" name="id" value="<?php echo $activity->activityID; ?>"></input>
							<button type="submit" class="btn btn-primary" name="deleteActivity">Delete</button>
						</form>
                    </td>
                </tr>

                <?php
                        }                        
                    }
                ?>
            </tbody>
        </table>
    </div>


    <h4> Add New Activity </h4>
    
    <br />

    <div id="googleMap" style="height: 250px; "></div>

    <br />

    <form action="../../includes/register.inc.php" method="POST" autocomplete="off" enctype="multipart/form-data" id="activity-form">  
        <div class="form-group">
            <label for="exampleInputEmail1">Where?</label>
            <input type="text" class="form-control" name="location" placeholder="Where were you?">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">What did you do?</label>
            <textarea type="text" class="form-control" name="dsc" placeholder="What did you do?"></textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlFile1">Attach Image</label>
            <input type="file" class="form-control-file" name="image">
        </div>

        <input type="hidden" name="lat" value="NULL" id="lat">
        <input type="hidden" name="long" value="NULL" id="long">
        <button type="submit" class="btn btn-primary" name="submitActivity">Add Activity</button>
    </form>

    <br />
    <br />
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>