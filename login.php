<?php
    $pageName = "Login";

    require_once "includes/header.inc.php";
?>

<div class="container">
    <br />
    <h1> Login </h1>
    

    <form action="includes/login.inc.php" method="POST" autocomplete="off">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="pwd" placeholder="Password">
        </div>

        <div class="form-group">    
            <label for="exampleInputPassword1">User Type</label>
            <select class="form-control form-control-sm" name="type">
                <option>Parent</option>
                <option>Student</option>
            </select>
        </div>

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remeber Me</label>
        </div>

        <button type="submit" class="btn btn-primary" name="submit">Login</button>
    </form>




    <a href="register">Register</a>
    <br />
    <a href="index">Home</a>
</div>

<?php
    require_once "includes/footer.inc.php";
?>