<?php
session_start();
require_once "dbh.inc.php";
require_once "classes.inc.php";

if (Input::exists('submitParent')) {
    $user = new ParentUser();

    try {
        $user->create('tblparent', array(
            'parentFirst' => $_POST['first'],
            'parentLast' => $_POST['last'],
            'parentEmail' => $_POST['email'],
            'parentPwd' => $_POST['pwd'] // PLEASE HASH THIS
        ), 'user');
        header("Location: ../registerBilling?Registration=success");

    } catch (Exception $e) {
        die($e->getMessage());
    }

} else if (Input::exists('submitStudent'))  {
    $user = new StudentUser();

    try {
        $user->create('tblstudent', array(
            'studentFirst' => $_POST['first'],
            'studentLast' => $_POST['last'],
            'studentEmail' => $_POST['email'],
            'studentPwd' => $_POST['pwd'],
            'parentID' => Session::get('user')
        ));
        header("Location: ../app/parent/accounts?Registration=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
} else if (Input::exists('submitCard')) {
    $card = new Billing();

    if (Session::exists('card')) {
        //update the card insead of creating a new one 
    } else {
        try {
            $card->create('tblbilling', array(
                'billingCardNumber' => $_POST['number'],
                'billingCardExpire' => $_POST['expire'],
                'billingCardName' => $_POST['name'],
                'parentID' => Session::get('user')
            ), 'card');

            header("Location: ../app/parent/dashboard?Registration=success");
        } catch (Exception $e) {
            die($e->getMessage());        
        }
    }
} else if (Input::exists('submitActivity')) {
    $activity = new Activity();

    $file = $_FILES['image'];
    $fileName = $_FILES['image']['name']; //to get the complete name of the file
    $fileTempName = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileError = $_FILES['image']['error'];
    $fileType = $_FILES['image']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt)); // get the extension of the file
    $allowedExt = array('jpg', 'png', 'jpeg');

    if (in_array($fileActualExt, $allowedExt)) {
        if ($fileError === 0) {
            $fileNameNew = uniqid('', true) . '.' . $fileActualExt;
            $fileDestination = '../uploads/' . $fileNameNew;
            
            try {
                $activity->create('tblactivity', array(
                    'activityLocation' => $_POST['location'],
                    'activityLatitude' => $_POST['lat'],
                    'activityLongitude' => $_POST['long'],
                    'activityDsc' => $_POST['dsc'],
                    'activityImage' => $fileNameNew,
                    'studentID' => Session::get('user')
                ));

                move_uploaded_file($fileTempName, $fileDestination);
                header("Location: ../app/student/dashboard?upload=success");
            } catch (Exception $e) {
                die($e->getMessage());
            }
            

        } else {
            header("Location: ../app/student/activities?upload=error");
        }
    } else {
        header("Location: ../app/student/activities?extension=invalid");
    }
} else if (Input::exists('submitReminderStudent')) {
    $reminder = new Reminder();

    try {
        $reminder->create('tblnotification', array(
            'notificationTime' => $_POST['time'],
            'notificationReminder' => $_POST['reminder'],
            'notificationDirection' => $_POST['direction'],
            'studentID' =>  Session::get('user')
        ));
        header("Location: ../app/student/notification?upload=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }

} else if (Input::exists('submitReminderParent')) {
    $reminder = new Reminder();

    try {
        $reminder->create('tblnotification', array(
            'notificationTime' => $_POST['time'],
            'notificationReminder' => $_POST['reminder'],
            'notificationDirection' => $_POST['direction'],
            'studentID' =>  $_POST['id']
        ));
        header("Location: ../app/parent/notification?upload=success");
    } catch (Exception $e) {
        die($e->getMessage());
    }
} else {
    header("Location: ../register.php?address=invalid");
}