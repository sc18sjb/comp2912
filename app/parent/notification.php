<?php
    $pageName = "parent | Accounts";
    $header = "parent";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $parent = new ParentUser();
        $user = $parent->getRows('tblParent', array(
            'parentID',
            '=',
            Session::get('user')
        ))[0];

        $accounts = $parent->getRows('tblstudent', array(
            'parentID',
            '=',
            $user->parentID
        ));
    } else {
        header("Location: ../../index?user=undefined");
    }
?>

<div class="container">
    <br />
    <h1><?php echo $user->parentFirst; ?>'s Activities</h1>

    <br />

    <?php
        if ($accounts) {
            if (count($accounts) > 1) {
    ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Select User</h5>
            <form action="" method="GET" autocomplete="off">
                <div class="form-group">
                    <select class="form-control" name="user">
                        <?php
                            foreach ($accounts as $int => $account) {
                        ?>

                        <option value="<?php echo $int; ?>"><?php echo $account->studentFirst; ?></option>
                        
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <button type="submitUser" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <br />

    <?php
            }
            $student = $accounts[Input::get('user', 0)];    
            
            $today = new DateTime(date('Y-m-d H:i:s'));

            $notification = new Reminder();
            $reminders = $notification->getRows('tblnotification', array(
                array('studentID', '=', $student->studentID),
                array('notificationDirection', '=', 'parent'),
                array('notificationTime', '>=', $today->format('Y-m-d'))
            ));

            if ($reminders) {                
    ?>
    
    <div class="table-reponsive">
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Notification</th>
                    <th scope="col">Deadline Date</th>
                </tr>
            </thead>

            <tbody>
                <?php
                    foreach ($reminders as $int => $reminder) {
                ?>
                <tr>
                    <th scope="row"><?php echo $int; ?></th>
                    <td><?php echo $reminder->notificationReminder; ?></td>
                    <td><?php echo $reminder->notificationTime ?></td>
                </tr>

                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>

    <?php
        } else {
    ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $student->studentFirst; ?> has no reminders!</h5>
                    <p class="card-text"><?php echo $student->studentFirst; ?> has not sent you any reminders! Send them one instead!</p>
                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                </div>
            </div>
            <br />
        </div>
    </div>

    <?php
        } 
    ?>

    <h4> Add New Reminder </h4>
    
    <br />


    <form action="../../includes/register.inc.php" method="POST" autocomplete="off">  
        <div class="form-group">
            <label for="exampleInputEmail1">Reminder</label>
            <input type="text" class="form-control" name="reminder" placeholder="Whats the reminder?">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">When For?</label>
            <input type="date" class="form-control" name="time" placeholder="##/##/##">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Who For?</label>
            <input type="text" class="form-control" placeholder="<?php echo $student->studentFirst . ' ' . $student->studentLast; ?>" readonly>
        </div>

        <input type="hidden" name="direction" value="student">
        <input type="hidden" name="id" value="<?php echo $student->studentID; ?>">
        <button type="submit" class="btn btn-primary" name="submitReminderParent">Add Activity</button>
    </form>

    <?php
        }
    ?>

    <br />
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>