-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 22, 2019 at 01:05 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studenttracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblactivity`
--

DROP TABLE IF EXISTS `tblactivity`;
CREATE TABLE IF NOT EXISTS `tblactivity` (
  `activityID` int(11) NOT NULL AUTO_INCREMENT,
  `activityLocation` varchar(256) NOT NULL,
  `activityDsc` text NOT NULL,
  `activityImage` varchar(256) NOT NULL,
  `studentID` int(11) NOT NULL,
  PRIMARY KEY (`activityID`),
  KEY `studentID` (`studentID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblactivity`
--

INSERT INTO `tblactivity` (`activityID`, `activityLocation`, `activityDsc`, `activityImage`, `studentID`) VALUES
(2, 'test', 'test', '5dd724a9e9e800.89989578.jpg', 21),
(3, 'test', 'i went to the place and killed someone', '5dd728267e6f39.71966844.jpg', 21),
(4, 'test', 'does this upload?', '5dd72cd80c20d7.06206363.jpg', 21),
(5, 'test', 'I went and raped someone', '5dd7dbe978b128.36232760.jpg', 23);

-- --------------------------------------------------------

--
-- Table structure for table `tblbilling`
--

DROP TABLE IF EXISTS `tblbilling`;
CREATE TABLE IF NOT EXISTS `tblbilling` (
  `billingID` int(11) NOT NULL AUTO_INCREMENT,
  `billingCardNumber` varchar(16) NOT NULL,
  `billingCardExpire` date NOT NULL,
  `billingCardName` varchar(64) NOT NULL,
  `parentID` int(11) NOT NULL,
  PRIMARY KEY (`billingID`),
  KEY `parentID` (`parentID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbilling`
--

INSERT INTO `tblbilling` (`billingID`, `billingCardNumber`, `billingCardExpire`, `billingCardName`, `parentID`) VALUES
(3, '12345', '0032-02-11', 'hi', 3),
(7, '123', '2000-06-03', 'SAMUEL J BARNES', 2),
(9, '123', '1961-06-03', 'KAREN S CUNT', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tblgrade`
--

DROP TABLE IF EXISTS `tblgrade`;
CREATE TABLE IF NOT EXISTS `tblgrade` (
  `moduleID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `grade` varchar(32) NOT NULL,
  `year` varchar(256) NOT NULL,
  PRIMARY KEY (`moduleID`,`studentID`),
  KEY `studentID` (`studentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblgrade`
--

INSERT INTO `tblgrade` (`moduleID`, `studentID`, `grade`, `year`) VALUES
(1, 21, '81%', '2019'),
(1, 23, '100%', '2019'),
(2, 21, 'test', '2019'),
(2, 23, 'FAIL', '2019'),
(3, 21, '100%', '2019'),
(3, 23, '100%', '2019'),
(4, 21, 'Pending', '2019'),
(4, 23, 'Pending', '2019'),
(5, 21, 'Pending', '2019'),
(5, 23, 'Pending', '2019'),
(6, 21, 'Pending', '2019'),
(6, 23, 'Pending', '2019');

-- --------------------------------------------------------

--
-- Table structure for table `tblmodule`
--

DROP TABLE IF EXISTS `tblmodule`;
CREATE TABLE IF NOT EXISTS `tblmodule` (
  `moduleID` int(11) NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(128) NOT NULL,
  `moduleCode` varchar(32) NOT NULL,
  `moduleLecturer` varchar(128) NOT NULL,
  `moduleTerm` int(11) NOT NULL,
  PRIMARY KEY (`moduleID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmodule`
--

INSERT INTO `tblmodule` (`moduleID`, `moduleName`, `moduleCode`, `moduleLecturer`, `moduleTerm`) VALUES
(1, 'Web Application Development', 'COMP2011', 'lowe, Amy, Dr', 1),
(2, 'Operating Systems', 'COMP2211', 'Wilson, Sam', 1),
(3, 'Numerical Computation ', 'COMP2421', 'Cohen, Netta, Prof', 1),
(4, 'Algorithms and Data Structures 1', 'COMP2711', 'Chakhlevitch, Natalia, Dr', 1),
(5, 'User Interfaces', 'COMP2811', 'Kelly, Thomas, Dr', 1),
(6, 'Software Engineering Principles ', 'COMP2912', 'Johnson, Owen', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblparent`
--

DROP TABLE IF EXISTS `tblparent`;
CREATE TABLE IF NOT EXISTS `tblparent` (
  `parentID` int(11) NOT NULL AUTO_INCREMENT,
  `parentFirst` varchar(32) NOT NULL,
  `parentLast` varchar(32) NOT NULL,
  `parentEmail` varchar(64) NOT NULL,
  `parentPwd` varchar(256) NOT NULL,
  PRIMARY KEY (`parentID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblparent`
--

INSERT INTO `tblparent` (`parentID`, `parentFirst`, `parentLast`, `parentEmail`, `parentPwd`) VALUES
(2, 'Sam', 'Barnes', 'barnes.samb@gmail.com', 'test'),
(3, 'dadan', 'minchia', 'davide.magagni@gmail.com', 'cicciamacchia'),
(4, 'hi', 'bello', 'davide.magagni@gmail.com', 'ciccia'),
(5, 'test', 'man', 'test@man.com', 'test'),
(6, 'Karen', 'Mclaren', 'karen@gmail.com', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

DROP TABLE IF EXISTS `tblstudent`;
CREATE TABLE IF NOT EXISTS `tblstudent` (
  `studentID` int(11) NOT NULL AUTO_INCREMENT,
  `studentFirst` varchar(32) NOT NULL,
  `studentLast` varchar(32) NOT NULL,
  `studentEmail` varchar(64) NOT NULL,
  `studentPwd` varchar(256) NOT NULL,
  `parentID` int(11) NOT NULL,
  PRIMARY KEY (`studentID`),
  KEY `parentID` (`parentID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`studentID`, `studentFirst`, `studentLast`, `studentEmail`, `studentPwd`, `parentID`) VALUES
(21, 'test', 'test', 'test@test.com', 'test', 6),
(23, 'bob', 'man', 'bob@man.com', 'test', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbltimetable`
--

DROP TABLE IF EXISTS `tbltimetable`;
CREATE TABLE IF NOT EXISTS `tbltimetable` (
  `timetableID` int(11) NOT NULL AUTO_INCREMENT,
  `timetableDate` date NOT NULL,
  `timetableStart` time NOT NULL,
  `timetableEnd` time NOT NULL,
  `timetableLecture` varchar(256) NOT NULL,
  `timetableLecturer` varchar(128) NOT NULL,
  `timetableLocation` varchar(256) NOT NULL,
  `studentID` int(11) NOT NULL,
  PRIMARY KEY (`timetableID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltimetable`
--

INSERT INTO `tbltimetable` (`timetableID`, `timetableDate`, `timetableStart`, `timetableEnd`, `timetableLecture`, `timetableLecturer`, `timetableLocation`, `studentID`) VALUES
(2, '2019-11-18', '11:00:00', '12:00:00', 'Algorithms 1', 'Chakhlevitch, Natalia, Dr', 'Roger Stevens LT 21 (8M.21)', 1),
(3, '2019-11-18', '15:00:00', '16:00:00', 'Numerical Computation', 'Cohen, Netta, Prof', 'Roger Stevens LT 21 (8M.21)', 1),
(4, '2019-11-18', '16:00:00', '17:00:00', 'Operating Systems', 'Wilson, Sam', 'Roger Stevens LT 21 (8M.21)', 1),
(5, '2019-11-19', '10:00:00', '11:00:00', 'Numerical Computation TuT', 'Cohen, Netta, Prof', 'Parkinson SR (1.08)', 1),
(6, '2019-11-21', '15:00:00', '16:00:00', 'Software Engineering Principles', 'Johnson, Owen', 'Roger Stevens LT 22 (10M.22)', 1),
(7, '2019-11-21', '16:00:00', '17:00:00', 'Operating Systems', 'Wilson, Sam', 'Roger Stevens LT 22 (10M.22)', 1),
(8, '2019-11-20', '10:00:00', '11:00:00', 'Numerical Computation', 'Cohen, Netta, Prof', 'Roger Stevens LT 21 (8M.21)', 1),
(9, '2019-11-20', '11:00:00', '12:00:00', 'User Interfaces', 'Kelly, Thomas, Dr', 'Roger Stevens LT 21 (8M.21)', 1),
(10, '2019-11-20', '12:00:00', '13:00:00', 'Software Engineering Principles', 'Johnson, Owen', 'Roger Stevens LT 21 (8M.21)', 1),
(11, '2019-11-22', '11:00:00', '12:00:00', 'Algorithms 1', 'Chakhlevitch, Natalia, Dr', 'Roger Stevens LT 22 (10M.22)', 1),
(12, '2019-11-22', '14:00:00', '15:00:00', 'User Interfaces', 'Kelly, Thomas, Dr', 'Michael Sadler RBLT (LG.X04)', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblactivity`
--
ALTER TABLE `tblactivity`
  ADD CONSTRAINT `tblactivity_ibfk_1` FOREIGN KEY (`studentID`) REFERENCES `tblstudent` (`studentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblbilling`
--
ALTER TABLE `tblbilling`
  ADD CONSTRAINT `tblbilling_ibfk_1` FOREIGN KEY (`parentID`) REFERENCES `tblparent` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblgrade`
--
ALTER TABLE `tblgrade`
  ADD CONSTRAINT `tblgrade_ibfk_1` FOREIGN KEY (`studentID`) REFERENCES `tblstudent` (`studentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tblgrade_ibfk_2` FOREIGN KEY (`moduleID`) REFERENCES `tblmodule` (`moduleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblstudent`
--
ALTER TABLE `tblstudent`
  ADD CONSTRAINT `tblstudent_ibfk_1` FOREIGN KEY (`parentID`) REFERENCES `tblparent` (`parentID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
