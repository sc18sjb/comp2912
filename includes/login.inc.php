<?php
session_start();
require_once "dbh.inc.php";
require_once "classes.inc.php";

if (Input::exists('submit')) {
    if ($_POST['type'] == 'Parent') {
        $parent = new ParentUser();

        if ($parent->login('tblparent', 
            array('parentEmail', '=', $_POST['email']),
            array('parentPwd', '=', $_POST['pwd'])
        )) {
            header("Location: ../app/parent/dashboard");
        } else {
            header("Location: ../login?login=failed");
        }

    } else if ($_POST['type'] == 'Student') {
        $student = new StudentUser();

        if ($student->login('tblstudent', 
            array('studentEmail', '=', $_POST['email']),
            array('studentPwd', '=', $_POST['pwd'])
        )) {
            header("Location: ../app/student/dashboard");
        } else {
            header("Location: ../login?login=failed");
        }


    } else {
        header("Location: ../login?type=invalid");
    }
}