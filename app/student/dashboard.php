<?php
    $pageName = "student | dashboard";
    $header = "student";

    require_once "../../includes/header.inc.php";

    if (Session::exists('user')) {
        $student = new StudentUser();
        $user = $student->getRows('tblstudent', array(
            'studentID',
            '=',
            Session::get('user')
        ))[0];

        $parent = new ParentUser();
        $parentUser = $parent->getRows('tblparent', array(
            'parentID',
            '=',
            $user->parentID
        ))[0];
    } else {
        header("Location: ../../index?user=undefined");
    }

    $times = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'];
    $days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
?>


<div class="container">
    <br />
    <h1><?php echo $user->studentFirst; ?>'s Dashboard </h1>
    <h5>Parent: <?php echo $parentUser->parentFirst . ' ' . $parentUser->parentLast; ?></h5>

    <br />


    <?php
        $today = new DateTime(date('Y-m-d H:i:s'));

        $notification = new Reminder();
        $reminders = $notification->getRows('tblnotification', array(
            array('studentID', '=', $user->studentID),
            array('notificationDirection', '=', 'student'),
            array('notificationTime', '>=', $today->format('Y-m-d'))
        ));

        if ($reminders) { 
    ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">You have notifications from <?php echo $parentUser->parentFirst; ?>!</h5>
            
            <div class="table-reponsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Notification</th>
                            <th scope="col">Deadline Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            foreach ($reminders as $int => $reminder) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $int; ?></th>
                            <td><?php echo $reminder->notificationReminder; ?></td>
                            <td><?php echo $reminder->notificationTime ?></td>
                        </tr>

                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <?php
        }
    ?>

    <br />

    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Your Grades</h5>

                    <div class="table responsive">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">Module Name</th>
                                    <th scope="col">Module Code</th>
                                    <th scope="col">Grade</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                
                            <tbody>
                                <?php
                                    $modules = new Grade();
                                    $grades = $modules->getRows('tblgrade', array(
                                        'studentID',
                                        '=',
                                        $user->studentID
                                    ));
                                    
                                    foreach ($grades as $int => $grade) {
                                        $module = $modules->getRows('tblmodule', array(
                                            'moduleID',
                                            '=',
                                            $grade->moduleID
                                        ))[0];
                                ?>

                                <tr>
                                    <td><?php echo $module->moduleName; ?></td>
                                    <td><?php echo $module->moduleCode; ?></td>
                                    
                                    <form action="../../includes/update.inc.php", method="POST", autocomplete="off">
                                        <td>
                                            <input type="text" class="form-control form-control-sm" name="grade" value="<?php echo $grade->grade; ?>">
                                        </td>

                                        <td>
                                            <input type="hidden" name="mid" value="<?php echo $grade->moduleID ?>"></input>
                                            <input type="hidden" name="sid" value="<?php echo $grade->studentID ?>"></input>
                                            <button type="submit" class="btn btn-primary" name="updateGrade">Update</button>
                                        </td>
                                    </form>
                                </tr>

                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Your Activities</h5>
                    <?php
                        $allActivities = new Activity();
                        $activities = $allActivities->getRows('tblactivity', array(
                            'studentID',
                            '=',
                            $user->studentID
                        ));

                        if (!$activities) {
                    ?>

                    <p class="card-text">You have not added any activities to your account yet! Let your parents know what you've been up to.</p>
                    <a href="activities" class="btn btn-primary">Add Activities</a>

                    <?php
                        } else {
                            $activity = $activities[count($activities) - 1];
                    ?>

                    <p class="card-text">Recently you've been</p>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Where you were</th>
                                    <th scope="col">What you did</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <th scope="row"></th>
                                    <td scope="col"><?php echo $activity->activityLocation; ?></td>
                                    <td scope="col"><?php echo $activity->activityDsc; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <a href="activities" class="btn btn-primary">Add More Activities</a>

                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <br />

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Your Timetable</h5>
            <?php
                include "../../includes/timetable.inc.php";
            ?>  
        </div>
    </div>

    <br />
</div>

<?php
    require_once "../../includes/footer.inc.php";
?>