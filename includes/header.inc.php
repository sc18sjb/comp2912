<?php
	session_start();
	date_default_timezone_set('Europe/London');
	
	require_once "dbh.inc.php";
	require_once "classes.inc.php";

	$webName = "studentTracker";

	if (!isset($pageName))
		$pageName = "UNDEFINED";

	if (!isset($header)) 
		$header = "";
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width = device-width, initial-scale = 1">
		<title>Student Tracker | <?php echo $pageName; ?></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/comp2912/static/css/main.css?version=0">
	</head>
	
	<body>	

		<?php 
			if ($header == "parent") {
		?>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="dashboard"><?php echo $webName; ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/parent/dashboard">Dashboard</a>
					</li>		

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/parent/activities">Activities</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/parent/notification">Notifications</a>
					</li>
				</ul>

				<ul class="navbar-nav" style="float: right">
					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/parent/billing">Billing</a>
					</li>	

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/parent/accounts">Accounts</a>
					</li>
					
					<li class="nav-item ">
   				 		<form action="../../includes/logout.inc.php", method="POST", autocomplete="off">
							<button type="submit" class="btn btn-primary" name="submitLogout">Logout</button>
						</form>
					</li>	
				</ul>
			</div>
		</nav>
		<?php 
			} else if ($header == "student") {
		?>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="dashboard"><?php echo $webName ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/student/dashboard">Dashboard</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/student/activities">Activities</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/app/student/notification">Notifications</a>
					</li>
				</ul>

				<ul class="navbar-nav" style="float: right">
					<li class="nav-item ">
   				 		<form action="../../includes/logout.inc.php", method="POST", autocomplete="off">
							<button type="submit" class="btn btn-primary" name="submitLogout">Logout</button>
						</form>
					</li>	
				</ul>
			</div>
		</nav>
		<?php
			} else {
		?>	
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="index"><?php echo $webName; ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="/comp2912/index">Home <span class="sr-only">(current)</span></a>
					</li>	

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/login">Login</a>
					</li>	

					<li class="nav-item">
						<a class="nav-link" href="/comp2912/register">Register</a>
					</li>
				</ul>
			</div>
		</nav>
		<?php 
			}
		?>