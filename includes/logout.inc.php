<?php
session_start();
require_once "dbh.inc.php";
require_once "classes.inc.php";

if (Input::exists('submitLogout')) {
    $_SESSION = array();
    session_destroy();
    header("Location: ../index?logout=success");
} else {
    header("Location: ../app/parent/dashboard?logout=error");
}